import { createSlice } from '@reduxjs/toolkit';

export interface UserState {
    accessToken: string;
    uid: string;
    displayName?: string;
    name?: string;
    username?: string;
    email?: string;
    emailValid?: boolean;
    phoneNumber?: string;
    photoURL?: string | null;
}

export interface NotificationState {
    message: string;
    created_at: string;
}

interface initialState {
    user: UserState | null;
    notifications: NotificationState[];
}

const initialState: initialState = {
    user: null,
    notifications: [],
};

/**
 * Create a slice as a reducer containing actions.
 *
 * In this example actions are included in the slice. It is fine and can be
 * changed based on your needs.
 */
export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUser: (state, action) => {
            state.user = action.payload;
        },
        login: (state, action) => {
            state.user = action.payload;
        },
        logout: (state) => {
            state.user = null;
        },
        setNotifications: (state, action) => {
            state.notifications = action.payload;
        },
        // setUsername: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState.username>) => {
        //     state.username = action.payload;
        // },
        // setEmail: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState.email>) => {
        //     state.email = action.payload;
        // },
        // setPhone: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState.phone>) => {
        //     state.phone = action.payload;
        // },
        // setProfileImage: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState.profileImage>) => {
        //     state.profileImage = action.payload;
        // },
    },
});

// A small helper of user state for `useSelector` function.
export const getUser = (state: any) => {
    if (state.user) {
        return state.user.user;
    }
};

// Exports all actions
export const { setUser, login, logout, setNotifications } = userSlice.actions;

export default userSlice.reducer;
