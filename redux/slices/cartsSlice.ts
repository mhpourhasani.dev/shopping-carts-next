import { createSlice } from '@reduxjs/toolkit';

export interface ProductsState {
    id: string;
    name: string;
    brand: string;
    price?: number;
    percentDiscount?: number;
    isFavorite: boolean;
    images?: string[];
    sizes?: number[];
    categories?: string[];
}

export interface BrandsState {
    name: string;
    image: string;
}

interface initialState {
    loading: boolean;
    carts: ProductsState[];
}

const initialState: initialState = {
    loading: false,
    carts: [],
};

/**
 * Create a slice as a reducer containing actions.
 *
 * In this example actions are included in the slice. It is fine and can be
 * changed based on your needs.
 */
export const cartsSlice = createSlice({
    name: 'carts',
    initialState,
    reducers: {
        addToCarts: (state, action) => {
            state.carts.push(action.payload);
        },
        setCarts: (state, action) => {
            state.carts = action.payload;
        },
    },
});

// Exports all actions
export const { addToCarts, setCarts } = cartsSlice.actions;

export default cartsSlice.reducer;
