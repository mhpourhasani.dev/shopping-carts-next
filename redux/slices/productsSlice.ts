import { createSlice } from '@reduxjs/toolkit';

export interface ProductsState {
    id?: string;
    name: string;
    brand: string;
    price?: number;
    percentDiscount?: number;
    isFavorite: boolean;
    images?: string[];
    sizes?: number[];
    categories?: string[];
    services?: string[];
}

export interface BrandsState {
    name: string;
    image: string;
}

interface initialState {
    loading: boolean;
    products: ProductsState[];
    brands: BrandsState[];
}

const initialState: initialState = {
    loading: false,
    products: [],
    brands: [],
};

/**
 * Create a slice as a reducer containing actions.
 *
 * In this example actions are included in the slice. It is fine and can be
 * changed based on your needs.
 */
export const productSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
        setProducts: (state, action) => {
            state.products = action.payload;
        },
        setBrands: (state, action) => {
            state.brands = action.payload;
        },
    },
});

// Exports all actions
export const { setLoading, setProducts, setBrands } = productSlice.actions;

export default productSlice.reducer;
