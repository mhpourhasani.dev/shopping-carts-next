'use client';
import Categories from '@/components/Categories/Categories';
import Header from '@/components/Header/Header';
import MainBanners from '@/components/MainBanners/MainBanners';
import ProductsList from '@/components/Products/ProductsList';
import Search from '@/components/Search/Search';
import db from '@/firebase/firebase';
import { useAppDispatch, useAppSelector } from '@/redux/hooks';
import { setLoading, setProducts } from '@/redux/slices/productsSlice';
import { collection, getDocs } from 'firebase/firestore';
import { useEffect } from 'react';
import { toast } from 'react-toastify';

export default function Home() {
    const productsState = useAppSelector((state: any) => state.productsReducer.products);
    const loadingState = useAppSelector((state: any) => state.productsReducer.loading);
    const dispatch = useAppDispatch();
    const dbInstance = collection(db, 'shoes');

    const getData = async () => {
        dispatch(setLoading(true));
        await getDocs(dbInstance)
            .then((data) => {
                dispatch(
                    setProducts(
                        data.docs.map((item) => {
                            return { ...item.data(), id: item.id };
                        }),
                    ),
                );
                dispatch(setLoading(false));
            })
            .catch((error) => {
                console.log(error);
                 toast('The connection with the server is disconnected', { type: 'error', autoClose: 3000 });

            });
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <main className="w-full flex min-h-screen flex-col items-start p-4 pb-20 2xl:justify-center 2xl:items-center">
            <Header />
            <Search />
            <MainBanners />
            <Categories />
            <ProductsList products={productsState ? productsState : []} loading={loadingState} />
        </main>
    );
}
