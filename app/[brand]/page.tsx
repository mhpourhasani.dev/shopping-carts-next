'use client';
import ProductCardItem from '@/components/Products/ProductCardItem/ProductCardItem';
import db from '@/firebase/firebase';
import { collection, getDocs, query, where } from 'firebase/firestore';
import Link from 'next/link';
import { useParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import { toast } from 'react-toastify';

export default function Brands() {
    const [products, setProducts] = useState<any>({});
    const [isLoading, setIsLoading] = useState(true);

    const params = useParams();

    const findProductsBaseOnBrands = async (brandName: any) => {
        try {
            if (brandName) {
                const docSnap = await getDocs(query(collection(db, 'shoes'), where('brand', '==', brandName)));
                console.log(docSnap);

                // const result = await docSnap.get().then((querySnapshot: any[]) => {
                //     querySnapshot.forEach((doc) => {
                //         // Access each document's data
                //         console.log(doc.id, ' => ', doc.data());
                //     });
                // });

                // console.log(result);

                if (docSnap.docs) {
                    setIsLoading(false);
                    setProducts(
                        docSnap.docs.map((item) => {
                            return { ...item.data(), id: item.id };
                        }),
                    );
                } else {
                    // No such document!
                    toast('This item not found', { type: 'error', autoClose: 3000 });
                }
            }
        } catch (error) {
            toast(`Error fetching document: ${error}`, { type: 'error', autoClose: 3000 });
        }
    };

    useEffect(() => {
        const capsFirstLetter = params.brand.slice(0, 1)[0].toUpperCase();
        const otherWords = params.brand.slice(1);
        findProductsBaseOnBrands(capsFirstLetter + otherWords);
    }, [params.brand]);

    return (
        <section className="w-full flex flex-col min-h-screen gap-4 p-4 md:flex-1">
            <h1 className="text-3xl font-bold mb-5">{params.brand.slice(0, 1)[0].toUpperCase() + params.brand.slice(1)}</h1>

            <div>
                {!isLoading ? (
                    <div className="w-full grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6">
                        {products.length
                            ? products.map((product: any) => {
                                  return (
                                      <Link
                                          key={product.id}
                                          href={{
                                              pathname: `/${product.brand.toLocaleLowerCase()}/${product.name
                                                  .replace(' ', '-')
                                                  .toLocaleLowerCase()}`,
                                              query: { id: product.id },
                                          }}
                                          className="w-full"
                                      >
                                          <ProductCardItem product={product} />
                                      </Link>
                                  );
                              })
                            : null}
                    </div>
                ) : (
                    <section className="w-full grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6">
                        {Array.from(Array(6).keys()).map((_item, index) => (
                            <div key={index} className="w-full border p-1 rounded-lg">
                                <Skeleton width="100%" borderRadius="8px" className="aspect-square" />
                                <Skeleton width="70%" />
                                <Skeleton width="40%" />
                            </div>
                        ))}
                    </section>
                )}
            </div>
        </section>
    );
}
