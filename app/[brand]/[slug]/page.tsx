'use client';
import { useAppDispatch } from '@/redux/hooks';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import { useRouter, useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import arrowleft2Icon from '../../../assets/icons/svgs/arrowleft2.svg';
import arrowdownIcon from '../../../assets/icons/svgs/arrowdown.svg';
import loveIcon from '../../../assets/icons/svgs/love.svg';
import favoriteIcon from '../../../assets/icons/svgs/fill-favorite.svg';
import addIcon from '../../../assets/icons/svgs/add.svg';
import minusIcon from '../../../assets/icons/svgs/minus.svg';
import Image from 'next/image';
import PopOver from '@/components/PopOver/PopOver';
import { addToCarts } from '@/redux/slices/cartsSlice';
import { ProductsState } from '@/redux/slices/productsSlice';
import { toast } from 'react-toastify';
import { doc, getDoc } from 'firebase/firestore';
import db from '@/firebase/firebase';
import Head from 'next/head';
import Skeleton from 'react-loading-skeleton';

export default function SingleProduct() {
    const [product, setProduct] = useState<any>({
        id: '',
        isFavorite: false,
        brand: '',
        name: '',
        categories: [],
        images: [],
        percentDiscount: 0,
        price: 0,
        sizes: [],
        services: [],
    });
    const [tempProduct, setTempProduct] = useState<any>({});
    const [isLoading, setIsLoading] = useState(true);
    const dispatch = useAppDispatch();
    const router = useRouter();
    const searchParams = useSearchParams();

    const findProductByName = async (productID: string | null) => {
        try {
            if (productID) {
                const docRef = doc(db, 'shoes', productID);
                const docSnap = await getDoc(docRef);

                if (docSnap.exists()) {
                    setProduct(docSnap.data());
                    setIsLoading(false);
                } else {
                    // No such document!
                    toast('This item not found', { type: 'error', autoClose: 3000 });
                }
            }
        } catch (error) {
            toast(`Error fetching document: ${error}`, { type: 'error', autoClose: 3000 });
        }
    };

    useEffect(() => {
        findProductByName(searchParams.get('id'));
    }, [searchParams]);

    const addToCartsHandler = (product: ProductsState) => {
        dispatch(addToCarts(product));
        toast('Product successfully added to cart', { type: 'success', autoClose: 3000 });
    };

    return (
        <>
            <Head>
                <title>{`${product.brand} - ${product.name}`}</title>
            </Head>

            <section className="w-full p-4 pb-20">
                <div className="flex justify-between w-full items-center">
                    <span onClick={() => router.back()} className="bg-bg-2 w-10 h-10 flex justify-center items-center rounded-full">
                        <Image src={arrowleft2Icon} alt="arrow left" className="w-5 h-auto" />
                    </span>
                    <span className="bg-bg-2 p-2 w-10 h-10 flex justify-center items-center rounded-full">
                        {
                            <Image
                                src={product?.isFavorite ? favoriteIcon : loveIcon}
                                alt="favorite"
                                onClick={() => setProduct({ ...product, isFavorite: !product.isFavorite })}
                            />
                        }
                    </span>
                </div>

                <div className="flex w-full items-center justify-center mt-6 aspect-square md:w-10/12 lg:w-5/12 xl:max-w-lg">
                    <Swiper
                        slidesPerView={1}
                        spaceBetween={10}
                        loop={true}
                        pagination={{
                            clickable: true,
                        }}
                        navigation={true}
                        className="w-full"
                    >
                        {isLoading ? (
                            <SwiperSlide className="">
                                <Skeleton width="100%" borderRadius="8px" className="aspect-square" />
                            </SwiperSlide>
                        ) : (
                            product?.images?.map((image: any, index: number) => {
                                return (
                                    <SwiperSlide key={index} className="">
                                        <Image
                                            src={image}
                                            alt={product?.name}
                                            width={500}
                                            height={500}
                                            className="h-full w-full rounded-lg"
                                        />
                                    </SwiperSlide>
                                );
                            })
                        )}
                    </Swiper>
                </div>

                <span className="flex flex-col gap-2 my-4">
                    <span className="font-bold text-customBlack-100 text-xl">
                        {isLoading ? <Skeleton width="40vw" borderRadius="8px" /> : `${product?.name}`}
                    </span>
                    <span className="text-primary-100 font-bold text-lg">
                        {isLoading ? <Skeleton width="20vw" borderRadius="8px" /> : `$ ${product?.price}`}
                    </span>
                </span>

                <section className="flex flex-col gap-2">
                    <PopOver
                        popoverTitle={
                            <div className="rounded-full w-full h-12 bg-bg-2 flex justify-between items-center px-4">
                                <span>Size</span>
                                <span className="flex items-center gap-7">
                                    <span className="font-semibold">
                                        {isLoading ? (
                                            <Skeleton width="50px" borderRadius="8px" className="py-1" />
                                        ) : (
                                            `$ ${tempProduct?.size}`
                                        )}
                                    </span>
                                    <Image src={arrowdownIcon} alt="arrow-down" />
                                </span>
                            </div>
                        }
                        popoverContent={
                            <div className="w-full overflow-y-auto flex flex-col gap-2 p-4 pb-20">
                                {product?.sizes?.map((size: number) => {
                                    return (
                                        <span
                                            key={size}
                                            onClick={() => setTempProduct({ ...tempProduct, size: size })}
                                            className={`w-full bg-bg-2 rounded-full px-4 py-2.5 ${
                                                size === tempProduct.size && 'bg-primary-100 text-white font-semibold'
                                            }`}
                                        >
                                            {size}
                                        </span>
                                    );
                                })}
                            </div>
                        }
                    />

                    <div className="rounded-full w-full h-12 bg-bg-2 flex justify-between items-center px-4">
                        <span>Color</span>
                        <span className="flex items-center gap-7">
                            <span>
                                {isLoading ? <Skeleton width="50px" borderRadius="8px" className="py-1" /> : `$ ${tempProduct?.size}`}
                            </span>
                            <Image src={arrowdownIcon} alt="arrow-down" />
                        </span>
                    </div>

                    <div className="rounded-full w-full h-12 bg-bg-2 flex justify-between items-center px-4">
                        <span>Quantity</span>
                        <div className="flex items-center gap-6">
                            <span
                                onClick={() => setTempProduct({ ...tempProduct, quantity: tempProduct.quantity + 1 })}
                                className="w-9 h-9 flex items-center justify-center bg-primary-100 rounded-full"
                            >
                                <Image src={addIcon} alt="add" />
                            </span>
                            <span>
                                {isLoading ? <Skeleton width="25px" borderRadius="8px" className="py-1" /> : `${tempProduct?.quantity}`}
                            </span>
                            <span className="w-9 h-9 flex items-center justify-center bg-primary-100 rounded-full">
                                <Image src={minusIcon} alt="minus" />
                            </span>
                        </div>
                    </div>
                </section>

                <div className="flex flex-wrap gap-1.5 my-6">
                    {isLoading ? (
                        <div className="flex flex-wrap gap-1.5">
                            {Array.from(Array(6).keys()).map((_item, index) => (
                                <div key={index} className="">
                                    <Skeleton width="70px" borderRadius="5px" className="py-1" />
                                </div>
                            ))}
                        </div>
                    ) : (
                        product?.categories?.map((category: string) => {
                            return (
                                <span key={category} className="text-gray-500 bg-bg-2 px-2 py-0.5 rounded-md">
                                    {category}
                                </span>
                            );
                        })
                    )}
                </div>

                {/* <p className="text-sm text-black-50">{product?.description}</p> */}

                <button
                    onClick={() => addToCartsHandler(product)}
                    className="w-full flex justify-between py-2.5 px-6 text-white rounded-full bg-primary-100"
                >
                    <span className="font-bold">
                        {isLoading ? <Skeleton width="20vw" borderRadius="8px" baseColor="#c084fc" /> : `$ ${product?.price}`}
                    </span>
                    <span className="font-light">Add to bag</span>
                </button>
            </section>
        </>
    );
}
