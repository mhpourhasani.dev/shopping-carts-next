'use client';
import ProductListItem from '@/components/Products/ProductListItem/ProductListItem';
import CustomButton from '@/components/common/CustomButton';
import { useAppDispatch, useAppSelector } from '@/redux/hooks';
import { ProductsState } from '@/redux/slices/productsSlice';
import Image from 'next/image';
import Link from 'next/link';
import arrowleft2Icon from '../../assets/icons/svgs/arrowleft2.svg';
import emptyCart from '../../assets/icons/svgs/cart-illustration.svg';
import { useRouter } from 'next/navigation';
import { setCarts } from '@/redux/slices/cartsSlice';

const CartsPage = () => {
    const cartsState = useAppSelector((state: any) => state.cartsReducer.carts);
    const dispatch = useAppDispatch();
    const router = useRouter();

    console.log(cartsState);

    return (
        <section className={`w-full flex flex-col min-h-screen gap-4 p-4 pb-20 md:flex-1`}>
            <span onClick={() => router.back()} className="bg-bg-2 w-10 h-10 flex justify-center items-center rounded-full">
                <Image src={arrowleft2Icon} alt="arrow left" className="w-5 h-auto" />
            </span>

            <div className="flex items-center justify-between text-customBlack-100">
                <h1 className="text-3xl font-bold mb-5">Carts</h1>
                {cartsState.length ? <span onClick={() => dispatch(setCarts([]))}>Remove All</span> : null}
            </div>

            {cartsState.length === 0 && (
                <div className="flex flex-col items-center justify-center gap-5 translate-y-1/2">
                    <Image src={emptyCart} alt="empty cart" />
                    <p className="text-2xl text-center">Your Cart is Empty</p>
                    <Link href={`/`}>
                        <CustomButton title="Explore Categories" className="!w-fit px-8 py-4" />
                    </Link>
                </div>
            )}

            {cartsState.length ? (
                <section className="flex flex-col flex-1 justify-between gap-4">
                    <div>
                        {cartsState.map((cart: ProductsState) => (
                            <div key={cart.id}>
                                <ProductListItem product={cart} />
                            </div>
                        ))}
                    </div>

                    <div className="w-full flex flex-col gap-3">
                        <span className="flex items-center justify-between">
                            <p>Subtotal</p>
                            <p>$ {cartsState.reduce((acc: any, curr: any) => acc + curr.quantity * curr.price, 0)}</p>
                        </span>
                        <span className="flex items-center justify-between">
                            <p>Shipping Cost</p>
                            <p>$</p>
                        </span>
                        <span className="flex items-center justify-between">
                            <p>Tax</p>
                            <p>$</p>
                        </span>
                        <span className="flex items-center justify-between">
                            <p>Total</p>
                            <p>$</p>
                        </span>

                        <CustomButton title="Checkout" className="mt-5" />
                    </div>
                </section>
            ) : null}
        </section>
    );
};

export default CartsPage;
