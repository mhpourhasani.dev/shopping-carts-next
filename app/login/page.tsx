'use client';
import { useState } from 'react';
import CustomInput from '@/components/common/CustomInput';
import CustomButton from '@/components/common/CustomButton';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '@/firebase/firebase';
import { login, setNotifications } from '@/redux/slices/userSlice';
import { useAppDispatch, useAppSelector } from '@/redux/hooks';
import { useRouter } from 'next/navigation';
import { toast } from 'react-toastify';
import Image from 'next/image';
import loginImage from '@/assets/images/login-page.svg';
import appleLogo from '@/assets/icons/svgs/apple-logo.svg';
import googleLogo from '@/assets/icons/svgs/google-logo.svg';
import Link from 'next/link';
import Head from 'next/head';

const LoginPage = () => {
    const [formData, setFormData] = useState({ email: '', password: '' });
    const [formDataError, setFormDataError] = useState({ email: '', password: '' });
    const userState = useAppSelector((state: any) => state.userReducer.user);
    const dispatch = useAppDispatch();
    const router = useRouter();

    const changeHandler = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const loginHandler = (e: any) => {
        e.preventDefault();

        if (!formData.email) {
            setFormDataError({ ...formDataError, email: 'Please enter your email' });
        } else if (!formData.password) {
            setFormDataError({ ...formDataError, password: 'Please enter your password' });
        } else if (formData.password.length < 8) {
            setFormDataError({ ...formDataError, password: 'password must be 8 character' });
        } else {
            signInWithEmailAndPassword(auth, formData.email, formData.password)
                .then((userAuth) => {
                    // store the user's information in the redux state
                    dispatch(
                        login({
                            email: userAuth.user.email,
                            uid: userAuth.user.uid,
                            displayName: userAuth.user.displayName,
                            photoUrl: userAuth.user.photoURL,
                        }),
                    );
                    toast('Welcome to MHP shop', { type: 'success', autoClose: 3000 });
                    router.push('/');
                })
                .catch((error) => {
                    toast(error, { type: 'error', autoClose: 3000 });
                });
        }
    };

    return (
        <section className="w-full min-h-screen flex md:items-center md:gap-10">
            <Head>
                <title>Login</title>
            </Head>

            <div className="hidden md:flex md:items-center md:justify-center md:h-screen md:flex-1 md:bg-bg-2">
                <Image src={loginImage} alt="login image" className="w-full h-auto 2xl:w-9/12" />
            </div>

            <div className="w-full flex flex-col gap-4 p-4 md:flex-1 md:p-0">
                <h1 className="text-3xl font-bold mb-5">Login</h1>
                <CustomInput
                    label="Email"
                    name="email"
                    placeholder="example@gmail.com"
                    value={formData.email}
                    onChange={changeHandler}
                    error={formDataError.email}
                    inputClassName="focus:border-primary-100 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
                />
                <CustomInput
                    label="Password"
                    name="password"
                    value={formData.password}
                    onChange={changeHandler}
                    error={formDataError.password}
                    inputClassName="focus:border-primary-100 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
                />
                <CustomButton
                    title="Login"
                    disabled={!formData.email || !formData.password}
                    onClick={loginHandler}
                    className="disabled:bg-gray-300 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
                />

                <div className="flex gap-2">
                    <span>Dont have an Account?</span>
                    <Link href="/signup" className="font-semibold">
                        Create One
                    </Link>
                </div>

                <div className="w-full flex items-center gap-2 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]">
                    <hr className="flex-1 border border-gray-200" />
                    <span className="-translate-y-0.5">or</span>
                    <hr className="flex-1 border border-gray-200" />
                </div>

                <div className="flex flex-col gap-4 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]">
                    <span className="grid grid-cols-4 py-3 px-5 bg-bg-2 rounded-full cursor-pointer">
                        <Image src={appleLogo} alt="apple" />
                        <p className="col-span-2">Login with Apple</p>
                    </span>
                    <span className="grid grid-cols-4 py-3 px-5 bg-bg-2 rounded-full cursor-pointer">
                        <Image src={googleLogo} alt="google" />
                        <p className="col-span-2">Login with Google</p>
                    </span>
                </div>
            </div>
        </section>
    );
};

export default LoginPage;
