import NavBar from '@/components/NavBar/NavBar';
import './globals.css';
import type { Metadata } from 'next';
import { Providers } from '@/redux/provider';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-loading-skeleton/dist/skeleton.css';

export const metadata: Metadata = {
    title: 'Shopping Cart',
    description: 'This in proffesenial shopping cart website',
    applicationName: 'Shopping Cart',
    viewport: 'initial-scale=1.0, width=device-width',
    authors: [{ name: 'MH Pourhasani', url: 'https://mh-pourhasani.vercel.app/' }],
    themeColor: '#9747FF',
    keywords: ['Shopping Cart', 'MHP Shop', 'shoes', 'shop', 'shoe', 'adidas', 'nike', 'ecco', 'reebok'],
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
    return (
        <html lang="en">
            <body className="w-full flex flex-col justify-center items-center bg-white">
                <ToastContainer />
                <Providers>
                    {children}
                    <NavBar />
                </Providers>
            </body>
        </html>
    );
}
