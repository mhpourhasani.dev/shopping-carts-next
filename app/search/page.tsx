'use client';
import { useAppSelector } from '@/redux/hooks';
import Image from 'next/image';
import { useRouter, useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import searchIcon from '../../assets/icons/svgs/search.svg';
import notProductSearchFound from '../../assets/icons/svgs/search-illustration.svg';
import ProductListItem from '@/components/Products/ProductListItem/ProductListItem';
import CustomButton from '@/components/common/CustomButton';
import Link from 'next/link';

const SearchPage = () => {
    const productsState = useAppSelector((state: any) => state.productsReducer.products);
    const [filteredProducts, setFilteredProducts] = useState([]);
    const [searchValue, setSearchValue] = useState('');
    const searchParams = useSearchParams().get('q');
    const router = useRouter();

    useEffect(() => {
        if (!searchParams) {
            setFilteredProducts(productsState);
        } else {
            setFilteredProducts(productsState.filter((p: any) => p.name.toLocaleLowerCase().includes(searchParams?.toLocaleLowerCase())));
        }
    }, [searchParams, productsState]);

    useEffect(() => {
        setSearchValue(searchParams ? searchParams : '');
    }, [searchParams]);

    const changeHandler = (e: any) => {
        setSearchValue(e.target.value);
    };

    const inputKeyDown = (e: any) => {
        if (e.keyCode === 13) {
            router.push(`?q=${searchValue}`);
        }
    };

    return (
        <section
            className={`w-full flex flex-col ${filteredProducts.length === 0 && 'justify-between'} min-h-[66.6vh] gap-4 p-4 md:flex-1`}
        >
            <div>
                <h1 className="text-3xl font-bold mb-5">Search</h1>

                <span className="relative w-full h-12 flex flex-col items-center">
                    <Image src={searchIcon} alt="search" className="absolute left-4 top-1/4 w-5 h-auto" />
                    <input
                        type="search"
                        placeholder="Search"
                        value={searchValue}
                        onChange={changeHandler}
                        onKeyDown={inputKeyDown}
                        className="rounded-full w-full pl-12 pr-4 placeholder:text-black-100 text-black-100 h-12 text-sm bg-bg-2 outline-none"
                    />
                </span>
            </div>

            {filteredProducts.length ? <span>{filteredProducts.length} Results Found</span> : null}

            {filteredProducts &&
                filteredProducts.map((p: any) => {
                    return <ProductListItem key={p.id} product={p} />;
                })}

            {filteredProducts.length === 0 && (
                <div className="flex flex-col items-center justify-center gap-5">
                    <Image src={notProductSearchFound} alt="Product not found" />
                    <p className="text-2xl text-center">Sorry, we couldn&apos;t find any matching result for your Search.</p>
                    <Link href={`/`}>
                        <CustomButton title="Explore Categories" className="!w-fit px-8 py-4" />
                    </Link>
                </div>
            )}
        </section>
    );
};

export default SearchPage;
