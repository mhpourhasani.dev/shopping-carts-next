'use client';
import { useState } from 'react';
import CustomInput from '@/components/common/CustomInput';
import CustomButton from '@/components/common/CustomButton';
import { auth } from '@/firebase/firebase';
import { useAppDispatch, useAppSelector } from '@/redux/hooks';
import { useRouter } from 'next/navigation';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { toast } from 'react-toastify';
import { login, setNotifications } from '@/redux/slices/userSlice';
import Image from 'next/image';
import signupImage from '@/assets/images/signup-page.svg';
import Link from 'next/link';
import Head from 'next/head';

const SignUpPage = () => {
    const [formData, setFormData] = useState({ email: '', password: '', confirmPassword: '', checkbox: false });
    const [formDataError, setFormDataError] = useState({ email: '', password: '', confirmPassword: '' });
    const userState = useAppSelector((state: any) => state.userReducer.user);
    const dispatch = useAppDispatch();
    const router = useRouter();

    const changeHandler = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const signupHandler = (e: any) => {
        e.preventDefault();

        if (!formData.email) {
            setFormDataError({ ...formDataError, email: 'Please enter your email' });
        } else if (!formData.password) {
            setFormDataError({ ...formDataError, password: 'Please enter your password' });
        } else if (formData.password.length < 8) {
            setFormDataError({ ...formDataError, password: 'password must be 8 character' });
        } else if (formData.password !== formData.confirmPassword) {
            setFormDataError({ ...formDataError, password: 'password not matched' });
        } else {
            createUserWithEmailAndPassword(auth, formData.email, formData.password)
                .then((userAuth) => {
                    dispatch(
                        login({
                            email: userAuth.user.email,
                            emailVerified: userAuth.user.emailVerified,
                            uid: userAuth.user.uid,
                        }),
                    );
                    toast('Welcome to Mhp shop', { type: 'success', autoClose: 3000 });
                    router.push('/');
                })
                .catch((error) => {
                    toast(error, { type: 'success', autoClose: 3000 });
                });
        }
    };

    return (
        <section className="w-full min-h-screen flex md:items-center md:gap-10">
            <Head>
                <title>Sign up</title>
            </Head>

            <div className="hidden md:flex md:items-center md:justify-center md:h-screen md:flex-1 md:bg-bg-2">
                <Image src={signupImage} alt="sign up image" />
            </div>

            <div className="w-full flex flex-col gap-4 p-4 md:flex-1 md:p-0">
                <h1 className="text-3xl font-bold mb-5">Sign Up</h1>
                <CustomInput
                    type="email"
                    label="Email"
                    name="email"
                    placeholder="example@gmail.com"
                    value={formData.email}
                    onChange={changeHandler}
                    error={formDataError.email}
                    inputClassName="focus:border-primary-100 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
                />
                <CustomInput
                    type="password"
                    label="Password"
                    name="password"
                    value={formData.password}
                    onChange={changeHandler}
                    error={formDataError.password}
                    inputClassName="focus:border-primary-100 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
                />
                <CustomInput
                    type="password"
                    label="Confirm Password"
                    name="confirmPassword"
                    value={formData.confirmPassword}
                    onChange={changeHandler}
                    error={formDataError.confirmPassword}
                    inputClassName="focus:border-primary-100 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
                />

                <span className="w-fit flex items-center gap-2">
                    <input
                        id="terms"
                        type="checkbox"
                        checked={formData.checkbox}
                        onChange={(e) => setFormData({ ...formData, checkbox: e.target.checked })}
                        className="w-5 h-5 outline-none accent-primary-100 cursor-pointer"
                    />
                    <label htmlFor="terms">Terms and Conditions</label>
                </span>

                <CustomButton
                    title="Sign Up"
                    onClick={signupHandler}
                    disabled={!formData.email || !formData.password || !formData.confirmPassword || !formData.checkbox}
                    className="focus:border-primary-100 disabled:bg-gray-300 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
                />

                <Link href="/login">I have an account</Link>
            </div>
        </section>
    );
};

export default SignUpPage;
