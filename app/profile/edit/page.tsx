'use client';
import { useState } from 'react';
import CustomInput from '@/components/common/CustomInput';
import CustomButton from '@/components/common/CustomButton';
import { useAppSelector } from '@/redux/hooks';
import { updateEmail, updateProfile } from 'firebase/auth';
import { auth } from '@/firebase/firebase';
import { toast } from 'react-toastify';
import { useRouter } from 'next/navigation';
import Head from 'next/head';

const EditPage = () => {
    const userState = useAppSelector((state: any) => state.userReducer.user);
    const [formData, setFormData] = useState({
        name: userState?.displayName ? userState?.displayName : '',
        email: userState?.email ? userState?.email : '',
    });
    const [formDataError] = useState({ name: '', email: '' });
    const router = useRouter();

    const changeHandler = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const saveChangesHandler = () => {
        if (formData.name !== userState.displayName) {
            updateProfile(auth.currentUser!, {
                displayName: formData.name,
                photoURL: '',
            })
                .then(() => {
                    toast('Profile updated', { type: 'success', autoClose: 3000 });
                })
                .catch(() => {});
        }

        if (formData.email !== userState.email) {
            updateEmail(auth.currentUser!, formData.email)
                .then(() => {
                    toast('Email updated', { type: 'success', autoClose: 3000 });
                })
                .catch(() => {});
        }

        router.push('/profile');
    };

    return (
        <section className="w-full flex flex-col gap-4 p-4">
            <Head>
                <title>{userState?.displayName ? userState?.displayName : `MHP shop user`}</title>
            </Head>
            
            <CustomInput
                label="Name"
                name="name"
                value={formData.name}
                onChange={changeHandler}
                error={formDataError.name}
                inputClassName="focus:border-primary-100 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
            />
            <CustomInput
                label="Email"
                name="email"
                value={formData.email}
                onChange={changeHandler}
                error={formDataError.email}
                inputClassName="focus:border-primary-100 md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]"
            />

            <CustomButton title="Save Changes" onClick={saveChangesHandler} className="md:w-11/12 lg:w-9/12 xl:w-8/12 2xl:max-w-[600px]" />
        </section>
    );
};

export default EditPage;
