'use client';
import { auth } from '@/firebase/firebase';
import { useAppDispatch, useAppSelector } from '@/redux/hooks';
import { login, logout } from '@/redux/slices/userSlice';
import { onAuthStateChanged } from 'firebase/auth';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { useEffect } from 'react';
import appleLogo from '@/assets/icons/svgs/apple-logo.svg';
import Head from 'next/head';

const ProfilePage = () => {
    const userState = useAppSelector((state: any) => state.userReducer.user);
    const dispatch = useAppDispatch();
    const router = useRouter();

    useEffect(() => {
        onAuthStateChanged(auth, (userAuth) => {
            if (userAuth) {
                // user is logged in, send the user's details to redux, store the current user in the state
                dispatch(
                    login({
                        email: userAuth.email,
                        uid: userAuth.uid,
                        displayName: userAuth.displayName,
                        photoUrl: userAuth.photoURL,
                    }),
                );
            } else {
                dispatch(logout());
            }
        });
    }, [dispatch]);

    useEffect(() => {
        if (!userState) {
            return router.push('/login');
        }
    }, []);

    const logoutHandler = () => {
        dispatch(logout());
        auth.signOut();
        router.push('/');
    };

    return (
        <section className="w-full min-h-screen h-screen p-4 flex flex-col items-center">
            <Head>
                <title>{userState?.displayName ? userState?.displayName : `MHP shop user`}</title>
            </Head>

            <Image
                src={userState?.profileURL ? userState?.profileURL : appleLogo}
                alt={userState?.displayName ? userState?.displayName : 'image'}
                className="rounded-full shadow-md w-2/4 aspect-square"
            />
            <div className="bg-primary-100 bg-opacity-20 px-5 py-0.5 my-5 rounded-xl">
                {userState?.displayName ? userState.displayName : `user: ${userState?.uid}`}
            </div>

            <div className="w-full flex items-start justify-between bg-bg-2 rounded-xl p-2">
                <span className="flex flex-col gap-1">
                    <span className="font-bold text-lg">{userState?.displayName ? userState?.displayName : `MHP shop user`}</span>
                    <span className="text-gray-500">{userState?.email}</span>
                    <span className="text-gray-500">{userState?.phoneNumber}</span>
                </span>

                <Link href="/profile/edit" className="text-primary-100">
                    Edit
                </Link>
            </div>

            <div className="w-full">
                <button
                    type="submit"
                    className="w-full py-1.5 mb-4 border border-sky-600 rounded-lg text-sky-600 hover:bg-sky-500 hover:text-white"
                >
                    Change Password
                </button>

                <button
                    type="submit"
                    onClick={logoutHandler}
                    className="w-full py-1.5 border border-red-600 rounded-lg text-red-600 hover:bg-red-500 hover:text-white"
                >
                    Logout
                </button>
            </div>
        </section>
    );
};

export default ProfilePage;
