'use client';
import Image from 'next/image';
import notificationImage from '@/assets/icons/svgs/notificationPage.svg';
import Link from 'next/link';
import { useAppSelector } from '@/redux/hooks';
import Head from 'next/head';

const NotificationsPage = () => {
    const userState = useAppSelector((state: any) => state.userReducer.user);

    if (!userState?.notifications) {
        return (
            <div className="w-full flex flex-col justify-center p-4 md:p-0">
                <h1 className="text-3xl font-bold mb-5">Notifications</h1>
                <span className="flex flex-col items-center justify-center gap-8 translate-y-1/2">
                    <Image src={notificationImage} alt="notifications" className="w-1/2" />
                    <p>No Notification yet</p>
                    <Link href="/" className="py-3 px-5 rounded-full bg-primary-100 text-lg text-white">
                        Explore Categories
                    </Link>
                </span>
            </div>
        );
    }

    return (
        <section className="w-full flex flex-col min-h-screen gap-4 p-4 md:flex-1">
            <Head>
                <title>Notifications</title>
            </Head>

            <h1 className="text-3xl font-bold mb-5">Notifications</h1>

            <div className="w-full flex flex-col gap-2">
                {userState.notifications.map((notification: any) => {
                    <div className="w-full flex justify-between items-start rounded-xl bg-bg-2 p-4">
                        <span className="flex flex-1 flex-col gap-2">
                            <p>{notification.message}</p>
                            <p className="text-sm text-gray-400">{notification.created_at}</p>
                        </span>
                        <p>trash</p>
                    </div>;
                })}
            </div>
        </section>
    );
};

export default NotificationsPage;
