import { initializeApp, getApps } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { EmailAuthProvider } from 'firebase/auth';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
    apiKey: 'AIzaSyDc-4blOx-eNH56EYxTMJWpxbIBOoye8ig',
    authDomain: 'shopping-carts-next.firebaseapp.com',
    projectId: 'shopping-carts-next',
    storageBucket: 'shopping-carts-next.appspot.com',
    messagingSenderId: '574211686847',
    appId: '1:574211686847:web:03d873ea86fc47a860210a',
};

// Initialize Firebase
let app = getApps().length === 0 ? initializeApp(firebaseConfig) : getApps()[0];
const provider = new EmailAuthProvider();
const db = getFirestore(app);
const auth = getAuth(app);

export { provider, auth };
export default db;
