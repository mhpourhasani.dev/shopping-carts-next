import Link from 'next/link';
import ProductCardItem from './ProductCardItem/ProductCardItem';
import Skeleton from 'react-loading-skeleton';

interface Props {
    loading: boolean;
    products?: any[];
}

const ProductsList = ({ products, loading }: Props) => {
    return (
        <section className="w-full my-4 flex flex-col gap-4 2xl:justify-center 2xl:max-w-7xl">
            <span className="font-bold text-lg">Products</span>

            {!loading ? (
                <section className="w-full grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6">
                    {products?.map((product: any) => {
                        return (
                            <Link
                                key={product.id}
                                href={{
                                    pathname: `/${product.brand.toLocaleLowerCase()}/${product.name.replace(' ', '-').toLocaleLowerCase()}`,
                                    query: { id: product.id },
                                }}
                                className="w-full"
                            >
                                <ProductCardItem product={product} />
                            </Link>
                        );
                    })}
                </section>
            ) : (
                <section className="w-full grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6">
                    {Array.from(Array(6).keys()).map((_item, index) => (
                        <div key={index} className="w-full border p-1 rounded-lg">
                            <Skeleton width="100%" borderRadius="8px" className="aspect-square" />
                            <Skeleton width="70%" />
                            <Skeleton width="40%" />
                        </div>
                    ))}
                </section>
            )}
        </section>
    );
};

export default ProductsList;
