import Link from 'next/link';
import { ProductsState } from '@/redux/slices/productsSlice';
import Image from 'next/image';
import notImage from '../../../assets/images/not-images.svg';
import addIcon from '../../../assets/icons/svgs/add.svg';
import minusIcon from '../../../assets/icons/svgs/minus.svg';

interface Props {
    product: ProductsState;
}

const ProductListItem = ({ product }: Props) => {
    const { name, brand, price, images } = product;

    return (
        <section className="flex w-full items-center justify-between gap-3 p-2 rounded-lg bg-bg-2">
            <Link
                href={`/${brand?.toLocaleLowerCase()}/${name?.toLocaleLowerCase().replace(' ', '-')}`}
                className="h-24 w-24 hover:opacity-80 md:h-36 md:w-36 lg:h-40 lg:w-40"
            >
                <Image
                    src={images ? images[0] : notImage}
                    alt={name}
                    width={500}
                    height={500}
                    loading="lazy"
                    className="h-auto w-full rounded-md"
                />
            </Link>

            <div className="flex flex-1 flex-col items-center justify-between gap-3">
                <section className="w-full flex h-full items-center justify-between">
                    <Link href={`/${brand?.toLocaleLowerCase()}/${name?.toLocaleLowerCase().replace(' ', '-')}`}>
                        <span className="text-lg font-semibold hover:text-gray-600 md:text-xl lg:text-xl">{name}</span>
                    </Link>

                    <span className="font-semibold">${price}</span>
                </section>
                <div className="w-full flex items-center justify-between">
                    <div className="flex items-center gap-4 text-sm md:text-base">
                        <span className="flex items-center text-gray-400">
                            Size- <p className="text-customBlack-100 font-semibold">{price}</p>
                        </span>
                        <span className="flex items-center text-gray-400">
                            Color- <p className="text-customBlack-100 font-semibold">{price}</p>
                        </span>
                    </div>

                    <div className="flex items-center gap-1">
                        <span className="w-9 h-9 flex items-center justify-center bg-primary-100 rounded-full">
                            <Image src={addIcon} alt="add" />
                        </span>
                        <span className="w-9 h-9 flex items-center justify-center bg-primary-100 rounded-full">
                            <Image src={minusIcon} alt="minus" />
                        </span>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ProductListItem;
