'use client';
import { ProductsState } from '@/redux/slices/productsSlice';
import LoveIcon from '../../../assets/icons/svgs/love.svg';
import notImage from '../../../assets/images/not-images.svg';
import Image from 'next/image';

interface ProductsCardItemInterface {
    product: ProductsState;
}

const ProductCardItem = ({ product }: ProductsCardItemInterface) => {
    const { name, price, percentDiscount, images } = product;

    return (
        <section className="relative w-full rounded-lg bg-bg-2">
            <Image src={LoveIcon} alt="favorite" className="absolute w-5 h-auto top-3 right-3" />

            <span className="w-full aspect-square">
                <Image src={images ? images[0] : notImage} width={500} height={500} alt={name} className="w-full rounded-t-lg" />
            </span>

            <div className="flex flex-col gap-1.5 p-1.5">
                <span className="font-bold">{name}</span>
                <div className="flex gap-3">
                    <span className="text-gray-900 font-bold">$ {price}</span>
                    <span className="text-gray-600 font-medium line-through">
                        $ {price && percentDiscount && price / (percentDiscount * 100)}
                    </span>
                </div>
            </div>
        </section>
    );
};

export default ProductCardItem;
