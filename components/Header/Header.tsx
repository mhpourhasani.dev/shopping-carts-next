'use client';
import Image from 'next/image';
import bagIcon from '../../assets/icons/svgs/bag.svg';
import Link from 'next/link';
import { useAppSelector } from '@/redux/hooks';
import ProfileIcon from '@/assets/icons/profileIcon';

const Header = () => {
    const userState = useAppSelector((state: any) => state.userReducer.user);

    return (
        <header className="flex justify-between items-center w-full pb-5">
            <Link
                href={userState ? '/profile' : '/login'}
                className="w-10 h-10 flex justify-center items-center rounded-full p-1.5 bg-bg-2"
            >
                <ProfileIcon/>
                {/* <Image src={profileIcon} alt="bag" className="w-full h-full" /> */}
            </Link>

            <span className="font-bold text-xl">MHP Shop</span>

            <Link href="/carts" className="w-10 h-10 flex justify-center items-center p-2 rounded-full bg-primary-100">
                <Image src={bagIcon} alt="bag" className="w-full h-full" />
            </Link>
        </header>
    );
};

export default Header;
