'use client';
import Image from 'next/image';
import { useAppDispatch, useAppSelector } from '@/redux/hooks';
import { collection, getDocs } from 'firebase/firestore';
import db from '@/firebase/firebase';
import { useEffect, useState } from 'react';
import { BrandsState, setBrands } from '@/redux/slices/productsSlice';
import Skeleton from 'react-loading-skeleton';
import Link from 'next/link';
import { toast } from 'react-toastify';

const Categories = () => {
    const brandsState = useAppSelector((state: any) => state.productsReducer.brands);
    const dispatch = useAppDispatch();
    const dbInstance = collection(db, 'brand logos');
    const [isLoading, setIsLoading] = useState(false);

    const getData = async () => {
        setIsLoading(true);
        await getDocs(dbInstance)
            .then((data) => {
                setIsLoading(false);
                dispatch(
                    setBrands(
                        data.docs.map((item) => {
                            return { ...item.data() };
                        }),
                    ),
                );
            })
            .catch((error) => {
                toast('The connection with the server is disconnected', { type: 'error', autoClose: 3000 });
            });
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <section className="w-full flex flex-col gap-4 my-5">
            <span className="font-bold text-lg">Categories</span>
            {!isLoading ? (
                <div className="w-full flex overflow-x-auto gap-2">
                    {brandsState.map((brand: BrandsState) => {
                        return (
                            <div key={brand.name} className="">
                                <Link
                                    href={`/${brand.name.toLowerCase()}`}
                                    className="w-24 flex flex-col items-center justify-center gap-2"
                                >
                                    <span className="w-full flex justify-center items-center border border-bg-2 rounded-full aspect-square">
                                        <Image
                                            src={brand.image}
                                            alt={brand.name.toLowerCase()}
                                            width={500}
                                            height={500}
                                            className="w-20 h-20 object-contain p-1.5"
                                        />
                                    </span>
                                    <span>{brand.name}</span>
                                </Link>
                            </div>
                        );
                    })}
                </div>
            ) : (
                <section className="w-full flex gap-2 overflow-x-auto">
                    {Array.from(Array(4).keys()).map((_item, index) => (
                        <div key={index} className="w-full flex flex-col justify-center gap-2">
                            <Skeleton width="6rem" borderRadius="50%" className="aspect-square" />
                            <Skeleton width="60%" className="translate-x-1/3" />
                        </div>
                    ))}
                </section>
            )}
        </section>
    );
};

export default Categories;
