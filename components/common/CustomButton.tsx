'use client';

interface ButtonProps {
    title: string;
    onClick?: any;
    disabled?: boolean;
    className?: string;
}

const CustomButton = ({ title, onClick, disabled, className }: ButtonProps) => {
    return (
        <button onClick={onClick} disabled={disabled} className={`w-full text-white bg-primary-100 rounded-full py-3 ${className}`}>
            {title}
        </button>
    );
};

export default CustomButton;
