'use client';

interface InputProps {
    label?: string;
    type?: string;
    name?: string;
    value?: string;
    placeholder?: string;
    onChange: any;
    inputClassName?: string;
    error?: string;
}

const CustomInput = ({ label, type = 'text', name, value, placeholder, onChange, inputClassName, error }: InputProps) => {
    return (
        <div className="w-full flex flex-col gap-2">
            <label htmlFor={name || 'input'}>{label}</label>
            <input
                id={name || 'input'}
                type={type}
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={onChange}
                className={`px-2 p-3 border rounded-xl outline-none focus:outline-none ${inputClassName}`}
            />
            {error && <span className="text-red-600">{error}</span>}
        </div>
    );
};

export default CustomInput;
