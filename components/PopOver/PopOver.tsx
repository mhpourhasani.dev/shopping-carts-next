import { Popover, Transition } from '@headlessui/react';
import { Fragment } from 'react';

interface PopoverProps {
    popoverTitle: any;
    popoverTitleClassName?: string;
    popoverContent: any;
    popoverContentClassName?: string;
}

const PopOver = ({ popoverTitle, popoverTitleClassName, popoverContent, popoverContentClassName }: PopoverProps) => {
    return (
        <section className="h-auto w-full font-iranyekan sm:hidden">
            <Popover className="w-full overflow-y-auto">
                <>
                    <Popover.Button
                        dir="ltr"
                        className={`flex items-center w-full justify-center h-auto border-1 border-secondary300 rounded-lg outline-none focus:outline-none ${popoverTitleClassName}`}
                    >
                        {popoverTitle}
                    </Popover.Button>

                    <>
                        <Transition.Child
                            as={Fragment}
                            enter="ease-in-out"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="ease-in-out"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <div className="fixed inset-0 z-30 w-screen h-screen bg-gray-900 bg-opacity-50" />
                        </Transition.Child>

                        <Transition
                            as={Fragment}
                            enter="transition ease-out duration-200"
                            enterFrom="opacity-0 translate-y-full duration-300"
                            enterTo="opacity-100 h-auto translate-y-0"
                            leave="transition ease-in duration-150"
                            leaveFrom="opacity-100 translate-y-0"
                            leaveTo="opacity-0 translate-y-full duration-300"
                        >
                            <Popover.Panel
                                className={`fixed w-full h-96 bottom-0 left-0 z-40 flex justify-center items-center rounded-t-3xl bg-white p-4 ${popoverContentClassName}`}
                            >
                                {popoverContent}
                            </Popover.Panel>
                        </Transition>
                    </>
                </>
            </Popover>
        </section>
    );
};

export default PopOver;
