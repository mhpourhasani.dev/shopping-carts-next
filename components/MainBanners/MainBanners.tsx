"use client"
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import Image from 'next/image';

const MainBanners = () => {
    const banners = [
        { id: '1', image: '' },
        { id: '2', image: '' },
        { id: '3', image: '' },
        { id: '4', image: '' },
        { id: '5', image: '' },
        { id: 'main-banner', image: '' },
    ];

    return (
        <section className="w-full bg-sky-200 rounded-xl aspect-video">
            <Swiper
                slidesPerView={1}
                spaceBetween={10}
                loop={true}
                pagination={{
                    clickable: true,
                }}
                navigation={true}
                className="w-full"
            >
                {banners.map((banner, index) => {
                    return (
                        <SwiperSlide key={index} className="flex items-center justify-start">
                            <Image src={banner.image} alt={banner.image} className="rounded-xl aspect-video" />
                        </SwiperSlide>
                    );
                })}
            </Swiper>
        </section>
    );
};

export default MainBanners;
