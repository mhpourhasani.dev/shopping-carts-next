'use client';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import HomeIcon from '@/assets/icons/homeIcon';
import NotificationIcon from '@/assets/icons/notificationbingIcon';
import ReceiptIcon from '@/assets/icons/receiptIcon';
import ProfileIcon from '@/assets/icons/profileIcon';

const NavBar = () => {
    const pathname = usePathname();

    const navBarItems = [
        { title: 'Home', icon: <HomeIcon />, href: '/' },
        { title: 'Notification', icon: <NotificationIcon />, href: '/notifications' },
        { title: 'Receipt', icon: <ReceiptIcon />, href: '/receipt' },
        { title: 'Profile', icon: <ProfileIcon />, href: '/profile' },
    ];

    return (
        <nav className="flex w-full fixed justify-around items-center bottom-0 bg-white z-50 h-16">
            {navBarItems.map((navItem) => {
                const isActive = pathname?.startsWith(navItem.href);

                return (
                    <Link
                        className={isActive ? 'bg-primary-100 rounded-full w-9 h-9 flex justify-center items-center' : ''}
                        href={navItem.href}
                        key={navItem.title}
                    >
                        {navItem.icon}
                    </Link>
                );
            })}
        </nav>
    );
};

export default NavBar;
