'use client';
import Image from 'next/image';
import searchIcon from '../../assets/icons/svgs/search.svg';
import { useEffect, useState } from 'react';
import { useAppSelector } from '@/redux/hooks';
import { useRouter } from 'next/navigation';

const Search = () => {
    const productsState = useAppSelector((state: any) => state.productsReducer.products);
    const [searchValue, setSearchValue] = useState('');
    const [filteredProducts, setFilteredProducts] = useState([]);
    const router = useRouter();

    useEffect(() => {
        setFilteredProducts(productsState);
    }, [productsState]);

    const changeHandler = (e: any) => {
        setSearchValue(e.target.value);
    };

    const searchResult = () => {
        if (productsState) {
            return productsState.filter(() => productsState.includes(searchValue.toLowerCase()));
        }
    };

     if (searchValue && !filteredProducts) {
         return <p className="mt-1 absolute top-full rounded-xl w-full shadow-lg">No Item matched...</p>;
     }

    const inputKeyDown = (e: any) => {
        if (e.keyCode === 13) {
            router.push(`/search?q=${searchValue}`);
        }
    };

    return (
        <section className="w-full flex flex-col justify-center my-2 relative items-center">
            <div className="w-full">
                <Image src={searchIcon} alt="search" className="absolute left-4 top-1/4 w-5 h-auto" />
                <input
                    type="search"
                    placeholder="Search"
                    value={searchValue}
                    onChange={changeHandler}
                    onKeyDown={inputKeyDown}
                    className="rounded-full w-full pl-12 pr-4 placeholder:text-black-100 text-black-100 h-12 text-sm bg-bg-2 outline-none"
                />
            </div>

            <div className="w-full bg-white z-50 max-h-60">
                {searchValue && filteredProducts.length && (
                    <p className="mt-1 top-full rounded-xl w-full overflow-y-auto">{searchResult()}</p>
                )}
            </div>
        </section>
    );
};

export default Search;
